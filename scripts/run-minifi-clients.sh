# Script to execute a command on all MiNiFi client nodes
if [ $# != 4 ]
then
        echo "Arguments needed: file with MiNiFi client nodes, identity file, sleep time, command to run"
        exit 1
fi

clients_file=$1
identity_file=$2
sleep_time=$3
command_to_run=$4
bash ./sleep_time.sh $sleep_time
retn_val=$?
IFS="\r\n" GLOBIGNORE='*' command eval "CLIENTS=($(cat $clients_file))"
iter=0
num_clients=${#CLIENTS[@]}
if [ "$retn_val" == "0" ]
then
        while [ "$iter" -lt "$num_clients" ]
        do
		ssh -i "$identity_file" ${CLIENTS[$iter]} $command_to_run	
                iter=`expr $iter + 1`
        done
else
        echo "Couldn't run command for MiNiFi client"
fi
