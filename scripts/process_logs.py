#!/usr/bin/py
import sys
import statistics

file_name=sys.argv[1]
latencies=[]
with open(file_name,'r') as fp:
	line=fp.readline()
	while line:
		if "elapsed" in line and "Key" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			elapsed_value=float(split_line[1])
			print(elapsed_value)
			latencies.append(elapsed_value)
		line=fp.readline()

#Calculate bandwidth
flowfile_begin=0
flowfile_start=0
flowfile_end=0
flowfile_count=0
with open(file_name,'r') as fp:
	line=fp.readline()
	while line:
		if "m_flowfile_start" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			flowfile_start=int(split_line[1])
			if flowfile_count==0:
				flowfile_begin=flowfile_start
			flowfile_count+=1
		if "m_now" in line:
			line=fp.readline()
			line=line.replace("'","")
			split_line=line.split()
			flowfile_end=int(split_line[1])
		line=fp.readline()
duration=float((flowfile_end-flowfile_begin))/1000 #divide to convert to seconds
#total_data=float(flowfile_count*19387429)/(1024*1024) #divide to convert to MB
#total_data=float(flowfile_count*1938798.8)/(1024*1024) #divide to convert to MB
total_data=float(flowfile_count*16814617)/(1024*1024) #divide to convert to MB
bandwidth=total_data/duration
print("Total run duration: ",str(duration))
print("Total data processed: ",str(total_data))
print("Actual Thruput: ",str(bandwidth)," MBps")
print("Expected Thruput per client: ","36.978586197 MBps")
avg_latency=sum(latencies)/len(latencies)
stdev_latency=statistics.stdev(latencies)
print("Latency per flow file: ",str(avg_latency),"+-",str(stdev_latency))
