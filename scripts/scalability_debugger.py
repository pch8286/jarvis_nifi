#!/bin/python
import sys
import re
from statistics import mean
import matplotlib
from matplotlib import pyplot as plt
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 22}

matplotlib.rc('font', **font)

file_name=sys.argv[1]
option=input("Enter latency to plot [Extraction,Filter,Groupby,GroupbyRecords,AggBarrier,AggGroupby,CountFlowFiles]: ")
grouping_latencies=[]
cleaned_latencies=[]
edge_epochs_observed={}
opId=None
threshold_latency=7000
with open(file_name, 'r') as fp:
	line = fp.readline()
	while line:
		if option=="Extraction":
			m=re.search('extraction time: (\d+)', line)
			if m:
				latency=int(m.group(1))
				grouping_latencies.append(latency)
				if latency <= threshold_latency:
					cleaned_latencies.append(latency)
		elif option=="CountFlowFiles":
			m=re.search('MyProcessor.extractRecords] record count is (\d+), with epoch ID: (\d+), edge ID: (\d+), and extraction time:', line)
			if m:
				epoch_id=int(m.group(2))
				edge_id=int(m.group(3))
				if epoch_id in edge_epochs_observed:
					edge_epochs_observed[epoch_id].append(edge_id)
				else:	
					edge_epochs_observed[epoch_id]=[]
					edge_epochs_observed[epoch_id].append(edge_id)
		elif option=="Filter":
			m=re.search('CustomFilterOperator.onComplete] LP Solver op id: (\d+), epoch duration is: (\d+), records', line)
			if m:
				latency=int(m.group(2))
				grouping_latencies.append(latency)
				if latency <= threshold_latency:
					cleaned_latencies.append(latency)
		elif option=="Groupby":
			m=re.search('CustomFullGroupbyOperator.onComplete] LP Solver op id: (\d+), epoch duration is: (\d+), records', line)
			if m:
				latency=int(m.group(2))
				grouping_latencies.append(latency)
				if latency <= threshold_latency:
					cleaned_latencies.append(latency)
		elif option=="GroupbyRecords":
			m=re.search('CustomFullGroupbyOperator.onComplete] LP Solver op id: (\d+), epoch duration is: (\d+), records: (\d+)', line)
			if m:
				records=int(m.group(3))
				grouping_latencies.append(records)
				cleaned_latencies.append(records)
		elif option=="AggBarrier":
			m=re.search('elapsed time at barrier: (\d+)', line)
			if m:
				latency=int(m.group(1))
				grouping_latencies.append(latency)
				if latency <= threshold_latency:
					cleaned_latencies.append(latency)
		elif option=="AggGroupby":
			#if opId is None:
			#	opId=input("Enter op ID of agg groupby operator:")
			#m=re.search('CustomFullGroupbyOperator.onComplete] LP Solver op id: ' + opId + ', epoch duration is: (\d+), records', line)
			m=re.search('CustomHashGlobalAggOperator.onComplete] LP Solver op id: (\d+), epoch duration is: (\d+), records', line)
			if m:
				latency=int(m.group(2))
				grouping_latencies.append(latency)
				if latency <= threshold_latency:
					cleaned_latencies.append(latency)
		line=fp.readline()
		
print("Displaying edge information with number of epochs received for each")
for epoch_id in edge_epochs_observed:
	print(epoch_id, len(edge_epochs_observed[epoch_id]))
	edge_epochs_observed[epoch_id].sort()
	for edges in edge_epochs_observed[epoch_id]:
		print(edges,",",end="")
	print("")

print(grouping_latencies[:50])
print("Number of epochs processed by operator is:" + str(len(grouping_latencies)))
print(max(grouping_latencies), mean(grouping_latencies), min(grouping_latencies))
plt.xlabel('Groupby operator instance Id on stream processor node')
plt.ylabel('Number of records per epoch')
plt.plot(cleaned_latencies)
plt.show()
