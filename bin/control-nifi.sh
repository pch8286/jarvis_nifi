mode=$1
if [ $mode = "stop" ] 
then
	./nifi.sh stop
else
	rm ../logs/nifi-app.log
	./nifi.sh start
fi
